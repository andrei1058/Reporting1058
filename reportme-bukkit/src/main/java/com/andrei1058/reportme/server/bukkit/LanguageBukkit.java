package com.andrei1058.reportme.server.bukkit;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.configuration.RMessages;
import com.andrei1058.reportme.server.ReportMeBukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class LanguageBukkit implements RMessages {

    private YamlConfiguration yml;
    private File file;

    private static LanguageBukkit messages;


    private LanguageBukkit() {
        messages = this;

        file = new File(ReportMeBukkit.plugin.getDataFolder(), "messages_en.yml");
        if (!file.exists()) {
            try {
                if (!file.createNewFile())
                    ReportMeBukkit.plugin.getLogger().severe("Could not create " + file.getPath());
            } catch (IOException e) {
                ReportMeBukkit.plugin.getLogger().severe("Could not create " + file.getPath());
                e.printStackTrace();
                return;
            }
        }

        yml = YamlConfiguration.loadConfiguration(file);
        yml.options().copyDefaults(true);
        yml.addDefaults(ReportsManager.getReportsManager().getDefaultMessages());
        yml.addDefault("moderator-notify-chat-hover", "&6Get vanished first!\n&fClick to teleport to the player.");

        save();

        ReportsManager.getReportsManager().getDefaultMessages().clear();
    }

    /**
     * Save changes.
     */
    private void save() {
        try {
            yml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialize language system.
     */
    public static void init() {
        if (messages == null) new LanguageBukkit();
    }


    @Override
    public String getMessage(IUser user, String path) {
        return ChatColor.translateAlternateColorCodes('&', yml.getString(path).replace("{prefix}", yml.getString("prefix")));
    }

    @Override
    public List<String> getMessages(IUser user, String path) {
        return yml.getStringList(path).stream().map(s -> ChatColor.translateAlternateColorCodes('&', s.replace("{prefix}", yml.getString("prefix")))).collect(Collectors.toList());
    }

    public static String getMessage(Player user, String path) {
        return ChatColor.translateAlternateColorCodes('&', messages.yml.getString(path).replace("{prefix}", messages.yml.getString("prefix")));
    }

    public static List<String> getMessages(Player user, String path) {
        return messages.yml.getStringList(path).stream().map(s -> ChatColor.translateAlternateColorCodes('&', s.replace("{prefix}", messages.yml.getString("prefix")))).collect(Collectors.toList());
    }

    public static LanguageBukkit getMessages() {
        return messages;
    }
}
