package com.andrei1058.reportme;

import com.andrei1058.reportme.report.ReportComment;
import org.jetbrains.annotations.Nullable;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public interface IUser {

    /**
     * Get user name.
     */
    String getName();

    /**
     * Get user uuid.
     */
    UUID getUUID();

    /**
     * Send a text message to the user.
     */
    void sendMessage(String message);

    /**
     * Send a text message to the user.
     */
    void sendMessage(String message, String hover, String click);

    /**
     * Send a list of messages.
     */
    void sendMessages(List<String> messages);

    /**
     * Send report info to the user.
     *
     *
     * @param moderator may be null if the report is not closed.
     * @param closeDate may be null if the report is not closed.
     */
    void sendReportInfo(int id, Timestamp openDate, int status, String reporterName, UUID reporter, String reportedName, UUID reported, String reason,
                        String server, String verdict, String moderatorName, @Nullable UUID moderator, @Nullable Timestamp closeDate, LinkedList<ReportComment> comments, int activeReports);

    /**
     * Check if the player has a permissions from the list.
     *
     * @return true if the user has one of the given permissions.
     */
    boolean hasPermission(String... permissions);

    /**
     * Check if user is online.
     */
    boolean isOnline();
}
