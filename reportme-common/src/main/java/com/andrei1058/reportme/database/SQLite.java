package com.andrei1058.reportme.database;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.commands.MyReports;
import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.report.ReportComment;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.report.ReportPreview;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.UUID;

import static com.andrei1058.reportme.database.Database.ReportStatus.ALL;

public class SQLite implements Database {

    private Connection connection;

    public SQLite() {

        // create db file
        File folder = new File("plugins/ReportMe"), f = new File(folder, "database.db");
        if (!folder.exists()) {
            if (!folder.mkdir())
                ReportsManager.getReportsManager().getServerSoftware().logError("Could not create " + folder.getPath());
        }

        if (!f.exists()) {
            try {
                if (!f.createNewFile())
                    ReportsManager.getReportsManager().getServerSoftware().logError("Could not create " + f.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // create connection
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + f.getAbsolutePath());
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        if (connection == null) return;

        // create default tables
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_reports (id INTEGER PRIMARY KEY AUTOINCREMENT, open_date TIMESTAMP NULL DEFAULT NULL, status INTEGER, " +
                    "reported_name VARCHAR(256), reported_uuid VARCHAR(256), reporter_name VARCHAR(256), reporter_uuid VARCHAR(256), reason VARCHAR(256), server VARCHAR(256), verdict VARCHAR(256)," +
                    " moderator_name VARCHAR(256), moderator_uuid VARCHAR(256), close_date TIMESTAMP, last_message TIMESTAMP, unread_messages INTEGER);");
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not create reportme_reports table!");
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_top (id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(256), uuid VARCHAR(256), times INTEGER(256));");
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not create reportme_top table!");
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_preferences (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid VARCHAR(256), setting VARCHAR(256), value VARCHAR(256));");
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not create reportme_preferences table!");
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_messages (id INTEGER PRIMARY KEY AUTOINCREMENT, report_id INTEGER, date TIMESTAMP, user_name VARCHAR(256), user_uuid VARCHAR(256), " +
                    "moderator INTEGER, message VARCHAR(256));");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getExisting(Report report) {
        try (PreparedStatement ps = connection.prepareStatement("SELECT id FROM reportme_reports WHERE reported_uuid = ? AND reporter_uuid = ? AND status != 0;")) {
            ps.setString(1, report.getReported().getUUID().toString());
            ps.setString(2, report.getReporter().getUUID().toString());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int saveNewReport(Report report) {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO reportme_reports (open_date, status, reported_name, reported_uuid, reporter_name, reporter_uuid, reason, server, last_message, unread_messages) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?);")) {
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setInt(2, 1);
            ps.setString(3, report.getReported().getName());
            ps.setString(4, report.getReported().getUUID().toString());
            ps.setString(5, report.getReporter().getName());
            ps.setString(6, report.getReporter().getUUID().toString());
            ps.setString(7, report.getReason());
            ps.setString(8, ReportsManager.getReportsManager().getServerSoftware().getServerName());
            ps.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
            ps.setInt(10, 0);
            if (ps.executeUpdate() == 0) {
                ReportsManager.getReportsManager().getServerSoftware().logError("Could not save report: " + report.getReported().getName() + " -> " + report.getReason());
            }

            try (ResultSet rs = connection.createStatement().executeQuery("SELECT MAX(id) FROM reportme_reports;")) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int increaseReportTimes(IUser user) {
        int reports = getReportedTimes(user.getUUID(), ALL);
        if (reports == 0) {
            try (PreparedStatement ps = connection.prepareStatement("INSERT INTO reportme_top (username, uuid, times) VALUES (?,?,?);")) {
                ps.setString(1, user.getName());
                ps.setString(2, user.getUUID().toString());
                ps.setInt(3, 1);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return reports + 1;
        }

        try {
            connection.createStatement().executeUpdate("UPDATE reportme_top SET times = times + 1 WHERE uuid = '" + user.getUUID().toString() + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reports + 1;
    }

    @Override
    public int getReportedTimes(UUID user, ReportStatus reportStatus) {
        int times = 0;
        if (reportStatus == ALL) {
            try (ResultSet rs = connection.createStatement().executeQuery("SELECT times FROM reportme_top WHERE uuid='" + user.toString() + "';")) {
                if (rs.next()) return rs.getInt(1);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            String pattern = "";
            if (reportStatus == ReportStatus.ACTIVE) pattern = "AND (status = '1' OR status = '2' OR status = '3')";
            if (reportStatus == ReportStatus.CLOSED) pattern = "AND (status = '0' OR status = '4')";
            try (ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM reportme_reports WHERE reported_uuid='" + user.toString() + "' " + pattern + ";")) {
                while (rs.next()) {
                    times++;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return times;
    }

    @Override
    public void commentReport(int id, IUser user, String message) {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO reportme_messages (date, user_name, user_uuid, moderator, message, report_id) VALUES (?,?,?,?,?,?);")) {
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setString(2, user.getName());
            ps.setString(3, user.getUUID().toString());
            ps.setInt(4, user.hasPermission("reportme.moderator") ? 1 : 0);
            ps.setString(5, message);
            ps.setInt(6, id);
            if (ps.executeUpdate() == 0) {
                ReportsManager.getReportsManager().getServerSoftware().logError("Could not save comment on report: " + id);
            }
            try (PreparedStatement sp = connection.prepareStatement("UPDATE reportme_reports SET last_message = ? WHERE id = ?;")) {
                sp.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                sp.setInt(2, id);
            }
            try (ResultSet rs = connection.prepareStatement("SELECT reporter_uuid FROM reportme_messages WHERE id = '" + id + "';").executeQuery()) {
                if (rs.next()) {
                    String uuid = rs.getString("reporter_uuid");
                    if (!uuid.equalsIgnoreCase(user.getUUID().toString())) {
                        connection.createStatement().executeUpdate("UPDATE reportme_reports SET unread_messages = unread_messages + 1 WHERE id = '" + id + "';");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResultSet getReport(int id) {
        try {
            return connection.createStatement().executeQuery("SELECT * FROM reportme_reports WHERE id = '" + id + "';");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public LinkedList<ReportComment> getComments(int reportID) {
        LinkedList<ReportComment> rc = new LinkedList<>();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM reportme_messages WHERE report_id='" + reportID + "';");
            while (rs.next()) {
                rc.add(new ReportComment(rs.getTimestamp("date"), rs.getString("user_name"), rs.getString("user_uuid"), rs.getInt("moderator"), rs.getString("message")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rc;
    }

    @Override
    public LinkedList<ReportPreview> getMyReports(IUser user, MyReports.ParamsCache cache) {
        LinkedList<ReportPreview> list = new LinkedList<>();
        try {
            String pattern = "";
            if (cache.getFilter() == ReportStatus.ACTIVE) pattern = " AND (status = '1' OR status = '2' OR status = '3')";
            if (cache.getFilter() == ReportStatus.CLOSED) pattern = " AND (status = '0' OR status = '4')";
            if (cache.getOrder() == Order.TIME_NEWER) pattern += " ORDER BY date(last_message) DESC";
            if (cache.getOrder() == Order.TIME_OLDER) pattern += " ORDER BY date(last_message) ASC";
            pattern += " LIMIT " + cache.getStart() + ", " + (cache.getStart()+ReportsManager.getReportsManager().getServerSoftware().getRConfig().getReportsPerPage()+1);
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM reportme_reports WHERE reporter_uuid = '" + user.getUUID() + "'" + pattern + ";");

            while (rs.next()) {
                list.add(new ReportPreview(rs.getInt("id"), rs.getInt("status"), rs.getString("reported_uuid"), rs.getString("reason"), rs.getString("reported_name"),
                        rs.getString("verdict"), rs.getTimestamp("open_date"), rs.getTimestamp("close_date"), rs.getInt("unread_messages"), rs.getTimestamp("last_message")
                        , rs.getString("moderator_uuid"), rs.getString("moderator_name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void markRead(int id) {
        try {
            connection.createStatement().executeUpdate("UPDATE reportme_reports SET unread_messages = 0 WHERE id = '" + id + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
