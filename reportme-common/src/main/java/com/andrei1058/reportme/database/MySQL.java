package com.andrei1058.reportme.database;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.commands.MyReports;
import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.report.ReportComment;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.configuration.RConfig;
import com.andrei1058.reportme.report.ReportPreview;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.UUID;

public class MySQL implements Database {

    private Connection connection;
    private RConfig config;

    public MySQL() {
        config = ReportsManager.getReportsManager().getServerSoftware().getRConfig();
        ReportsManager.getReportsManager().getServerSoftware().runAsync(this::connect);
    }

    /**
     * Create connection.
     */
    private void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + config.getDatabaseHost() + ":" + config.getDatabasePort() + "/" + config.getDatabaseName() + "?user=" + config.getDatabaseUser() +
                    "&password=" + config.getDatabasePassword() + "&autoReconnect=true&useSSL=" + config.isDatabaseSSL() + "&useUnicode=true&characterEncoding=UTF-8");
            ReportsManager.getReportsManager().database = this;
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not connect to MySQL database!");
            ReportsManager.getReportsManager().getServerSoftware().logError(e.getMessage());
            try {
                connection.close();
            } catch (SQLException ignore) {
            }
        }

        // create default tables
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_reports (id INT AUTO_INCREMENT PRIMARY KEY, open_date TIMESTAMP NULL DEFAULT NULL, status INT(3), " +
                    "reported_name VARCHAR(256), reported_uuid VARCHAR(256), reporter_name VARCHAR(256), reporter_uuid VARCHAR(256), reason VARCHAR(256), server VARCHAR(256), verdict VARCHAR(256)," +
                    " moderator_name VARCHAR(256), moderator_uuid VARCHAR(256), close_date TIMESTAMP);");
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not create reportme_reports table!");
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_top (id INT AUTO_INCREMENT PRIMARY KEY, username VARCHAR(256), uuid VARCHAR(256), times INT(256));");
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not create reportme_top table!");
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS reportme_preferences (id INT AUTO_INCREMENT PRIMARY KEY, uuid VARCHAR(256), setting VARCHAR(256), value VARCHAR(256));");
        } catch (SQLException e) {
            ReportsManager.getReportsManager().getServerSoftware().logError("Could not create reportme_preferences table!");
            e.printStackTrace();
        }
    }

    private boolean isClosed() {
        try {
            return connection == null || connection.isClosed();
        } catch (SQLException ignored) {
            return false;
        }
    }

    @Override
    public int getExisting(Report report) {
        return 0;
    }

    @Override
    public int saveNewReport(Report report) {
        return 0;
    }

    @Override
    public int increaseReportTimes(IUser user) {
        return 0;
    }

    @Override
    public int getReportedTimes(UUID user, ReportStatus reportStatus) {
        return 0;
    }

    @Override
    public void commentReport(int id, IUser user, String message) {

    }

    @Override
    public ResultSet getReport(int id) {
        return null;
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public LinkedList<ReportComment> getComments(int reportID) {
        return new LinkedList<>();
    }

    @Override
    public LinkedList<ReportPreview> getMyReports(IUser user, MyReports.ParamsCache cache) {
        LinkedList<ReportPreview> list = new LinkedList<>();
        return list;
    }

    @Override
    public void markRead(int id) {

    }
}
