package com.andrei1058.reportme.database;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.commands.MyReports;
import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.report.ReportComment;
import com.andrei1058.reportme.report.ReportPreview;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.UUID;

public interface Database {

    /**
     * @return -1 if could not found a report to merge.
     */
    int getExisting(Report report);

    /**
     * @return the report id.
     */
    int saveNewReport(Report report);

    /**
     * Increment user reported times.
     *
     * @return new reported times.
     */
    int increaseReportTimes(IUser user);

    /**
     * Get how many times the user was reported.
     */
    int getReportedTimes(UUID user, ReportStatus reportStatus);

    /**
     * Add new message to an existing open report.
     */
    void commentReport(int id, IUser user, String message);

    /**
     * Check if a report with the given ID exists.
     *
     * @return report details by id.
     */
    ResultSet getReport(int id);

    /**
     * Close database connection.
     */
    void close();

    LinkedList<ReportComment> getComments(int reportID);

    LinkedList<ReportPreview> getMyReports(IUser user, MyReports.ParamsCache cache);

    enum ReportStatus {
        ALL, ACTIVE, CLOSED
    }

    void markRead(int id);

    enum Order {
        //todo order bazat pe ultimul mesaj la report sau pe data inchiderii
        TIME_NEWER, TIME_OLDER;
    }
}
