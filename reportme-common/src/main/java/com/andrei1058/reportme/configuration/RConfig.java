package com.andrei1058.reportme.configuration;

import java.util.List;

public interface RConfig {

    String getDefaultLanguage();

    boolean isDatabaseEnabled();

    String getDatabaseHost();

    String getDatabaseName();

    String getDatabaseUser();

    String getDatabasePassword();

    String getDatabasePort();

    boolean isDatabaseSSL();

    boolean reportOffline();

    boolean isWebPanel();

    String panelLink();

    int suspectLevel();

    int getCooldown();

    boolean isMergeReports();

    int getCooldownSame();

    boolean acceptCustomReason();

    List<String> getReportTypes();

    int getReportsPerPage();
}
